import torch
import torch.nn as nn
import torch.nn.functional as F
from models.at_net import at_net
from torch.autograd import Variable
from models.PWCNet  import pwc_dc_net
from models.model_util import AttentionModule
import math

def flow_warp(x, flo):
    """
    warp an image/tensor (im2) back to im1, according to the optical flow
    x: [B, C, H, W] (im2)
    flo: [B, 2, H, W] flow
    """
    B, C, H, W = x.size()
    # mesh grid
    xx = torch.arange(0, W).view(1,-1).repeat(H,1)
    yy = torch.arange(0, H).view(-1,1).repeat(1,W)
    xx = xx.view(1,1,H,W).repeat(B,1,1,1)
    yy = yy.view(1,1,H,W).repeat(B,1,1,1)
    grid = torch.cat((xx,yy),1).float()
    if x.is_cuda:
        grid = grid.cuda()
    vgrid = Variable(grid) + flo
    # scale grid to [-1,1]
    vgrid[:,0,:,:] = 2.0*vgrid[:,0,:,:].clone() / max(W-1,1)-1.0
    vgrid[:,1,:,:] = 2.0*vgrid[:,1,:,:].clone() / max(H-1,1)-1.0

    vgrid = vgrid.permute(0,2,3,1)
    output = nn.functional.grid_sample(x, vgrid)
    mask = torch.autograd.Variable(torch.ones(x.size())).cuda()
    mask = nn.functional.grid_sample(mask, vgrid)

    mask[mask<0.9999] = 0
    mask[mask>0] = 1

    return output*mask

class ResBlock(nn.Module):
    def __init__(self, conv_dim):
        super(ResBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=conv_dim, out_channels=conv_dim,
                               kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(in_channels=conv_dim, out_channels=conv_dim,
                               kernel_size=3, stride=1, padding=1)
        self._initialize_weights()

    def forward(self, input):
        out = self.conv1(input)
        out = F.relu(out)
        out = self.conv2(out)
        out = input + out
        return out

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight,)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)






class SelfAttention(nn.Module):
    def __init__(self, n_embd, n_head):
        super().__init__()
        assert n_embd % n_head == 0
        # key, query, value projections for all heads
        self.key = nn.Linear(n_embd, n_embd)
        self.query = nn.Linear(n_embd, n_embd)
        self.value = nn.Linear(n_embd, n_embd)
        self.n_head = n_head

    def forward(self, x):
        B, T, C = x.size()
        # calculate query, key, values for all heads in batch and move head forward to be the batch dim
        k = self.key(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)
        q = self.query(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)
        v = self.value(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)
        # self-attend: (B, nh, T, hs) x (B, nh, hs, T) -> (B, nh, T, T)
        att = (q @ k.transpose(-2, -1)) * (1.0 / math.sqrt(k.size(-1)))
        att = F.softmax(att, dim=-1)
        y = att @ v # (B, nh, T, T) x (B, nh, T, hs) -> (B, nh, T, hs)
        y = y.transpose(1, 2).contiguous().view(B, T, C) # re-assemble all head outputs side by side
        return y


class Block(nn.Module):
    """ an unassuming Transformer block """

    def __init__(self, n_embd, n_head, ):
        super().__init__()
        self.attn = SelfAttention(n_embd, n_head)

    def forward(self, x):

        x = self.attn(x)
        return x


class GPT(nn.Module):
    def __init__(self, n_embd, n_head, n_layer, ):
        super().__init__()
        self.n_embd = n_embd
        self.blocks = nn.Sequential(*[Block(n_embd, n_head,)for layer in range(n_layer)])
        self._initialize_weights()

    def forward(self, image_tensor, thermal_tensor):

        bz = thermal_tensor.shape[0]
        h, w = thermal_tensor.shape[2:4]
        # forward the image model for token embeddings
        image_tensor = image_tensor.view(bz, 1, -1, h, w)
        thermal_tensor = thermal_tensor.view(bz, 1, -1, h, w)
        # pad token embeddings along number of tokens dimension
        token_embeddings = torch.cat([image_tensor, thermal_tensor], dim=1).permute(0,1,3,4,2).contiguous()
        token_embeddings = token_embeddings.view(bz, -1, self.n_embd) # (B, an * T, C)
        x = token_embeddings
        x = self.blocks(x) # (B, an * T, C)
        tensor_out = x.view(bz, -1, h, w).contiguous()

        return tensor_out


    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight,)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)




class TranformerFusionNet(nn.Module):
    def __init__(self, ):
        super(TranformerFusionNet, self).__init__()

        self.transformer1 = GPT(n_embd=64,
                                n_head=4,
                                n_layer=1,
                                )
        self.outputConv = nn.Conv2d(2, 1, kernel_size=1)
        self._initialize_weights()

    def forward(self, density_1,density_2):
        mean_tensor=(density_1+density_2)/2
        out = self.transformer1(density_1, density_2)
        out = mean_tensor+self.outputConv(out)
        return out

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight,)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

class FusionNet(nn.Module):
    def __init__(self, ):
        super(FusionNet, self).__init__()

        self.AttentionModule_1=AttentionModule(2,16)
        self.outputConv = nn.Conv2d(16, 1, kernel_size=1)
        self._initialize_weights()

    def forward(self, density_1,density_2):
        mean_tensor=(density_1+density_2)/2
        density_pair = torch.cat((density_1, density_2), dim=1)
        out = self.AttentionModule_1(density_pair)
        out = mean_tensor+self.outputConv(out)
        return out

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight,)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)


class FRCC(nn.Module):
    def __init__(self, device):
        super(FRCC, self).__init__()

        self.device=device
        #网络
        pwc_model_fn = './models/pwc_net.pth.tar'
        self.flow_net = pwc_dc_net(pwc_model_fn).cuda().eval()

        at_model_fn = './models/at_net.pth.tar'
        self.sccnet = at_net(load_weights=True,weight_path=at_model_fn).cuda().eval()
        self.fusion_net=TranformerFusionNet()

    def init_hidden(self,batch_size, img_height, img_width):
        #光流的归一化相关操作
        self.img_width = img_width
        self.img_height = img_height
        self.batch_size = batch_size

        self.preImg = torch.zeros([self.batch_size, 3, self.img_height, self.img_width]).to(self.device)
        self.estDensityMap = torch.zeros([self.batch_size, 1, self.img_height//8, self.img_width//8]).to(self.device)


    def forward(self, input, idx):

        img_pair = torch.cat((input, self.preImg), dim=1)
        if self.training==True:
            flow_map = self.flow_net(img_pair)[0]
        else:
            flow_map = self.flow_net(img_pair)*2.5

        flow_map = F.interpolate(flow_map, size=(self.img_height,self.img_width), mode="bilinear")

        self.warped_img=flow_warp(self.preImg, flow_map)
        relative_place_anno = F.interpolate(flow_map, scale_factor=0.125, mode="bilinear")
        warped_DensityMap = flow_warp(self.estDensityMap, relative_place_anno)
        #单人群计数网络
        csr_estDensityMap = self.sccnet(input)
        #融合网络
        self.preImg = input
        if(idx==0):
            self.estDensityMap = csr_estDensityMap
        else:
            # self.estDensityMap = csr_estDensityMap*0.4+warped_DensityMap*0.6
            # density_pair = torch.cat((csr_estDensityMap, warped_DensityMap), dim=1)
            self.estDensityMap = self.fusion_net(csr_estDensityMap,warped_DensityMap)
        return self.estDensityMap


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()
        self.net = nn.Sequential(
            nn.Conv2d(1, 64, kernel_size=3, padding=1),
            nn.LeakyReLU(0.2),

            nn.Conv2d(64, 64, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.2),

            nn.Conv2d(64, 128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2),

            nn.Conv2d(128, 128, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2),

            nn.Conv2d(128, 64, kernel_size=1),
            nn.LeakyReLU(0.2),
            nn.Conv2d(64, 1, kernel_size=1)
        )

    def forward(self, x):
        batch_size = x.size(0)
        return F.sigmoid(self.net(x).view(batch_size))

#用来做正则的，鼓励图像更加平滑
class TVLoss(nn.Module):
    def __init__(self, tv_loss_weight=1):
        super(TVLoss, self).__init__()
        self.tv_loss_weight = tv_loss_weight

    def forward(self, x):
        batch_size = x.size()[0]
        h_x = x.size()[2]
        w_x = x.size()[3]
        count_h = self.tensor_size(x[:, :, 1:, :])
        count_w = self.tensor_size(x[:, :, :, 1:])
        h_tv = torch.pow((x[:, :, 1:, :] - x[:, :, :h_x - 1, :]), 2).sum()
        w_tv = torch.pow((x[:, :, :, 1:] - x[:, :, :, :w_x - 1]), 2).sum()
        return self.tv_loss_weight * 2 * (h_tv / count_h + w_tv / count_w) / batch_size

    @staticmethod
    def tensor_size(t):
        return t.size()[1] * t.size()[2] * t.size()[3]


class GeneratorLoss(nn.Module):
    def __init__(self):
        super(GeneratorLoss, self).__init__()
        self.mse_loss = nn.MSELoss()
        self.tv_loss = TVLoss()

    def forward(self,estDensityMap, density_map, idx):

        if idx==0:
            loss_density = 0
            return loss_density
        else:
            loss_density = self.mse_loss(estDensityMap, density_map)
            tv_loss_density = self.tv_loss(estDensityMap)
            return loss_density+tv_loss_density