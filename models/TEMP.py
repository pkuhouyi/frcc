
class SelfAttention(nn.Module):
    def __init__(self, n_embd, n_head):
        super().__init__()
        assert n_embd % n_head == 0
        # key, query, value projections for all heads
        self.key = nn.Linear(n_embd, n_embd)
        self.query = nn.Linear(n_embd, n_embd)
        self.value = nn.Linear(n_embd, n_embd)
        self.n_head = n_head

    def forward(self, x):
        B, T, C = x.size()

        # calculate query, key, values for all heads in batch and move head forward to be the batch dim
        k = self.key(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)
        q = self.query(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)
        v = self.value(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)

        # self-attend: (B, nh, T, hs) x (B, nh, hs, T) -> (B, nh, T, T)
        att = (q @ k.transpose(-2, -1)) * (1.0 / math.sqrt(k.size(-1)))
        att = F.softmax(att, dim=-1)
        y = att @ v # (B, nh, T, T) x (B, nh, T, hs) -> (B, nh, T, hs)
        y = y.transpose(1, 2).contiguous().view(B, T, C) # re-assemble all head outputs side by side

        return y


class Block(nn.Module):
    """ an unassuming Transformer block """

    def __init__(self, n_embd, n_head, ):
        super().__init__()
        self.attn = SelfAttention(n_embd, n_head)

    def forward(self, x):

        x = self.attn(x)

        return x


class GPT(nn.Module):
    def __init__(self, n_embd, n_head, n_layer, ):
        super().__init__()
        self.n_embd = n_embd
        self.blocks = nn.Sequential(*[Block(n_embd, n_head,)for layer in range(n_layer)])
        self._initialize_weights()

    def forward(self, image_tensor, thermal_tensor):

        bz = thermal_tensor.shape[0]
        h, w = thermal_tensor.shape[2:4]
        # forward the image model for token embeddings
        image_tensor = image_tensor.view(bz, 1, -1, h, w)
        thermal_tensor = thermal_tensor.view(bz, 1, -1, h, w)
        # pad token embeddings along number of tokens dimension
        token_embeddings = torch.cat([image_tensor, thermal_tensor], dim=1).permute(0,1,3,4,2).contiguous()
        token_embeddings = token_embeddings.view(bz, -1, self.n_embd) # (B, an * T, C)
        x = token_embeddings
        x = self.blocks(x) # (B, an * T, C)
        tensor_out = x.view(bz, -1, h, w).contiguous()

        return tensor_out


    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight,)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)





class TranformerFusionNet(nn.Module):
    def __init__(self, ):
        super(TranformerFusionNet, self).__init__()

        self.transformer1 = GPT(n_embd=64,
                                n_head=4,
                                n_layer=1,
                                )
        self.outputConv = nn.Conv2d(2, 1, kernel_size=1)
        self._initialize_weights()

    def forward(self, density_1,density_2):
        mean_tensor=(density_1+density_2)/2
        out = self.transformer1(density_1, density_2)
        out = mean_tensor+self.outputConv(out)
        return out

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight,)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)