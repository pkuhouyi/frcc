import torch
import random

def get_min_size(batch_imgs,min_ht = 480,min_wd = 640):
    for img in batch_imgs:
        _,ht,wd = img.shape
        if ht<min_ht:
            min_ht = ht
        if wd<min_wd:
            min_wd = wd
    return min_ht,min_wd

def random_crop(dst_size, img, den, dot):
    _,h,w = img.shape
    factor=img.shape[1]//den.shape[1]
    x1 = random.randint(0, w - dst_size[1])
    y1 = random.randint(0, h - dst_size[0])
    x2 = x1 + dst_size[1]
    y2 = y1 + dst_size[0]
    label_x1 = x1//factor
    label_y1 = y1//factor
    label_x2 = x2//factor
    label_y2 = y2//factor
    return img[:,y1:y2,x1:x2], den[label_y1:label_y2,label_x1:label_x2], dot[label_y1:label_y2,label_x1:label_x2]

def collate_func(batch):
    '''
    将一个batch中的图像全部按照最小的大小来进行调整，注意dens和dots的大小是自适应的，可以有factor
    :param batch:
    :return:
    '''
    transposed = list(zip(*batch))
    imgs, dens ,dots= [transposed[0],transposed[1], transposed[2]]
    min_ht, min_wd = get_min_size(imgs)
    cropped_imgs = []
    cropped_dens = []
    cropped_dots = []
    for i in range(len(batch)):
        _img, _den, _dots = random_crop([min_ht,min_wd],imgs[i],dens[i],dots[i])
        cropped_imgs.append(_img)
        cropped_dens.append(_den)
        cropped_dots.append(_dots)

    cropped_imgs = torch.stack(cropped_imgs)
    cropped_dens = torch.stack(cropped_dens)
    cropped_dots = torch.stack(cropped_dots)
    return [cropped_imgs, cropped_dens, cropped_dots]










