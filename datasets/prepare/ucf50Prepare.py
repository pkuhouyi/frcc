import os
import scipy.io as sio
from datasets.prepare.utils.basePrepare import BasePrepare


class Ucf50Prepare(BasePrepare):
    def __init__(self,basepath,):

        images_path = os.path.join(os.path.abspath(basepath),'images/')
        gts_path = os.path.join(os.path.abspath(basepath),'ground-truth/')
        super(Ucf50Prepare, self).__init__(img_path=images_path,gts_path=gts_path,ext='*.jpg',is_multi=True, adj_num=3,zoom_para=0.3,down_sample_rate=8)

    def load_gt(self, gt_file_path):
        dataset = sio.loadmat(gt_file_path)
        locations=dataset['annPoints']
        return locations

    def get_gtfile_path(self,image_file_path, gt_file_base_path):
        filname=os.path.basename(image_file_path)
        j = filname.index('.jpg')
        substr = filname[0:j]
        gt_file_path = gt_file_base_path + substr + '_ann.mat'
        return gt_file_path

    def get_file_identifier(self,image_file_name):
        filname=os.path.basename(image_file_name)
        j = filname.index('.jpg')
        substr = filname[0:j]
        return substr



def generate():
    pre=Ucf50Prepare('/home/houyi/datasets/UCF_CC_50/')
    pre.process()


if __name__=="__main__":
    generate()











