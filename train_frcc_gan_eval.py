import os
import math
import time
import mmcv
import argparse
import torch.utils.data
from tqdm import tqdm
from datasets.crowdVideoDataset import CrowdVideoDataset,loader_wrapper
from torch.utils.data import DataLoader
from utils.utils import save_checkpoint,AverageMeter
from models.frcc_gan import FRCC,GeneratorLoss,Discriminator
from utils.logger import get_root_logger
from utils.seed import set_random_seed
import gc

parser = argparse.ArgumentParser(description='PyTorch CrowdCounting')
parser.add_argument('-train_path', default='/home/houyi/datasets/CrowdX_Video/train_data/ADJ3_ZOOM0.3_DR8_AUG18',)
parser.add_argument('-test_path', default='/home/houyi/datasets/CrowdX_Video/test_data/ADJ3_ZOOM0.3_DR8')
parser.add_argument('-lr',default=1e-6,type=float)
parser.add_argument('-scale_times',default=640.0,type=float)
# parser.add_argument('-resume_path',default='/home/houyi/Code/crowd_framework/work_dir/csrnet/20220219_142922.pth.tar',type=str)
parser.add_argument('-resume_path',default='',type=str)
parser.add_argument('-batch_size_train',default=5,type=int)
parser.add_argument('-batch_size_test',default=2,type=int)
parser.add_argument('-start_epoch',default=0,type=int)
parser.add_argument('-epochs',default=100,type=int)
parser.add_argument('-workers',default=10,type=int)
parser.add_argument('-task_id',default='frcc_double_eval',type=str)

args = parser.parse_args()

def train(train_loader, netG, netD, criterion, optimizerG, optimizerD, epoch):#在这里训练一个批次

    logger.info('Epoch: [{0}]\t'.format(epoch))
    lossesG = AverageMeter()
    lossesD = AverageMeter()

    losses_1 = AverageMeter()
    losses_2 = AverageMeter()
    losses_3 = AverageMeter()

    netG.train()
    netD.train()
    for imgs, density_maps, dot_maps in tqdm(train_loader):#[5，batch_size,channel,H,W]
        model.init_hidden(args.batch_size_train, img_height=384, img_width=512)
        idx = 0
        DLoss = 0
        GLoss = 0

        Loss1 = 0
        Loss2 = 0
        Loss3 = 0

        netG.zero_grad()
        for img, density_map, _ in zip(imgs, density_maps, dot_maps):
            img = img.to(device)
            density_map = density_map.to(device)*args.scale_times
            estDensityMap = netG(img,idx)

            realOut = netD(density_map).mean()
            fake_out = netD(estDensityMap).mean()
            DLoss += (1 - realOut + fake_out)*1000

            loss_mse,loss_adversarial,tv_loss= criterion(fake_out, estDensityMap, density_map, idx)

            GLoss+=loss_mse+loss_adversarial+tv_loss
            Loss1+=loss_mse
            Loss2+=loss_adversarial
            Loss3+=tv_loss
            idx += 1

        DLoss /= len(imgs)
        DLoss.backward(retain_graph=True)
        GLoss /= len(imgs)
        GLoss.backward()


        #注意这里的顺序是有讲究的，按照这种方式来操作
        optimizerD.step()
        optimizerG.step()
        lossesD.update(DLoss.item())
        lossesG.update(GLoss.item())
        GLoss /= len(imgs)

        Loss1 /= len(imgs)
        Loss2 /= len(imgs)
        Loss3 /= len(imgs)
        losses_1.update(Loss1.item())
        losses_2.update(Loss2.item())
        losses_3.update(Loss3.item())
        logger.info('LossG {lossG.val:.4f} ({lossG.avg:.4f})\t'
                    'LossD {lossD.val:.4f} ({lossD.avg:.4f})\t'
                    'Loss1 {Loss1.val:.4f} ({Loss1.avg:.4f})\t'
                    'Loss2 {Loss2.val:.4f} ({Loss2.avg:.4f})\t'
                    'Loss3 {Loss3.val:.4f} ({Loss3.avg:.4f})\t'.format(lossG=lossesG, lossD=lossesD,Loss1=losses_1,Loss2=losses_2,Loss3=losses_3))
        gc.collect()


def validate(val_loader, model):
    logger.info('begin test:')
    model.eval()
    mae1 = 0
    with torch.no_grad():
        for imgs, density_maps, dot_maps in tqdm(val_loader):#[5，batch_size,channel,H,W]
            model.init_hidden(args.batch_size_test, img_height=768, img_width=1024)
            idx = 0
            for img, density_map, _ in zip(imgs, density_maps, dot_maps):
                img = img.to(device)
                density_map = density_map.to(device)
                estDensityMap = model(img,idx)

                estDensityMap=estDensityMap*(1.0/args.scale_times)

                diffs1=estDensityMap.data.sum(dim=[1,2,3])-density_map.data.sum(dim=[1,2,3])

                diffs1=diffs1.cpu().numpy()
                mae1 += sum(abs(diffs1))

                idx += 1
    mae1 = mae1/(len(val_loader)*args.batch_size_test*5)
    logger.info(' * MAE_1 {mae1:.3f} '.format(mae1=mae1))
    return mae1


def adjust_learning_rate(optimizer, optimizerD, epoch):
    epochs_drop = 5
    args.lr = args.lr * math.pow(0.5, math.floor(epoch / epochs_drop))

    for param_group in optimizer.param_groups:
        param_group['lr'] = args.lr

    for param_group in optimizerD.param_groups:
        param_group['lr'] = args.lr


if __name__ == '__main__':
    # 环境准备
    args.work_dir = os.path.join('./work_dirs',args.task_id)
    mmcv.mkdir_or_exist(os.path.abspath(args.work_dir))
    timestamp = time.strftime('%Y%m%d_%H%M%S', time.localtime())
    log_file = os.path.join(args.work_dir, f'{timestamp}.log')
    logger = get_root_logger(log_file=log_file)
    set_random_seed(1000)
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


    ##数据集准备
    train_dataset=CrowdVideoDataset(args.train_path,train_flag=True)
    val_dataset=CrowdVideoDataset(args.test_path,train_flag=False)
    train_loader = DataLoader(train_dataset, batch_size=args.batch_size_train, num_workers=args.workers, shuffle=True, drop_last=True)
    val_loader = DataLoader(val_dataset, batch_size=args.batch_size_test,num_workers=args.workers)    #[video_seq_size,batch_size,channel_num,W,H]

    train_loader = loader_wrapper(train_loader)
    val_loader = loader_wrapper(val_loader)

    #模型准备阶段
    model = FRCC(device=device).cuda()
    netD=Discriminator().cuda()
    criterion = GeneratorLoss().cuda()

    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, weight_decay=1e-4)
    optimizerD = torch.optim.Adam(model.parameters(), lr=args.lr, weight_decay=1e-4)

    best_prec1 = 1e6
    if args.resume_path:
        checkpoint = torch.load(args.resume_path)
        args.start_epoch = checkpoint['epoch']
        best_prec1 = checkpoint['best_prec1']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        netD.load_state_dict(checkpoint['state_dict_netD'])
        optimizerD.load_state_dict(checkpoint['optimizer_netD'])
        logger.info("=> loaded checkpoint '{}' (epoch {})".format(args.resume_path, checkpoint['epoch']))

    for epoch in range(args.start_epoch, args.epochs):
        adjust_learning_rate(optimizer, optimizerD, epoch)

        train(train_loader, model, netD, criterion, optimizer, optimizerD, epoch)#在这里训练一个批次
        prec1 = validate(val_loader, model)
        is_best = prec1 < best_prec1
        best_prec1 = min(prec1, best_prec1)
        logger.info(' * best MAE {mae:.3f} '.format(mae=best_prec1))
        save_checkpoint({
            'epoch': epoch + 1,
            'best_prec1': best_prec1,
            'state_dict': model.state_dict(),
            'optimizer' : optimizer.state_dict(),
            'state_dict_netD': netD.state_dict(),
            'optimizer_netD' : optimizerD.state_dict(),
        }, is_best=is_best,saving_dir=args.work_dir,saving_prefix=timestamp)


