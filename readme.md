部署的时候关于correlation的安装https://github.com/NVIDIA/flownet2-pytorch/issues/227


# 1.这个是没有考虑Fusion_net的时候的情景
为了验证引入光流是有效果，特使用域间直接适应的方式来探究，单幅图像人群计数的算法是在shanghaitech_B数据集上训练的，然后直接在提出的CrowdX_video数据集上测试得到的结果，我们称这个过程为硬迁移

在这一个版本中，我自己训练了一个单张图像的计数网络，简称为at_net,还有直接调用了英伟达的一个光流估计算法，这两个模块摆脱了之前直接训练，模型难以收敛的问题，现在产生了挺好的效果

因为视频流数据是有时序限制的，但是也可以回过头来做处理，因为最后是算的平均数，所以无论是从视频内部算平均，还是最后一起算平均，结果都是一样的，也就是说结果可靠

在理论上，这种方法是有效果的，现在已经经过检验。
1.调节单张图计数网络和光流warp输出之间比例关系，得到
10：0    18.79
9：1     18.645
8：2     18.513
7：3     18.387
6：4     18.284
5：5     18.207
4：6     18.174
3：7     18.203
2：8     18.290
1：9     18.441
0：10    18.764


2.调节光流网络输出的光流的缩放倍数
500%    18.465
400%    18.266
350%    18.193
300%    18.139
250%    18.110
200%    18.112
150%    18.135
100%    18.174（也是上面1中的最好结果）
95%     18.180
90%     18.184
85%     18.188
80%     18.193

# 2.如下是考虑Fusion_net的情景
  ```python
class FusionNet(nn.Module):
    def __init__(self, ):
        super(FusionNet, self).__init__()
        self.AttentionModule_1=AttentionModule(2,16)
        self.outputConv = nn.Conv2d(16, 1, kernel_size=1)
        self._initialize_weights()

    def forward(self, density_1,density_2):
        mean_tensor=(density_1+density_2)/2
        density_pair = torch.cat((density_1, density_2), dim=1)
        out = self.AttentionModule_1(density_pair)
        out = mean_tensor+self.outputConv(out)
        return out
   ```
这个网络作为Fusionnet，最终的性能达到了14.274

```python
class SelfAttention(nn.Module):
    """
    A vanilla multi-head masked self-attention layer with a projection at the end.
    """

    def __init__(self, n_embd, n_head):
        super().__init__()
        assert n_embd % n_head == 0
        # key, query, value projections for all heads
        self.key = nn.Linear(n_embd, n_embd)
        self.query = nn.Linear(n_embd, n_embd)
        self.value = nn.Linear(n_embd, n_embd)
        self.n_head = n_head

    def forward(self, x):
        B, T, C = x.size()

        # calculate query, key, values for all heads in batch and move head forward to be the batch dim
        k = self.key(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)
        q = self.query(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)
        v = self.value(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)

        # self-attend: (B, nh, T, hs) x (B, nh, hs, T) -> (B, nh, T, T)
        att = (q @ k.transpose(-2, -1)) * (1.0 / math.sqrt(k.size(-1)))
        att = F.softmax(att, dim=-1)
        y = att @ v # (B, nh, T, T) x (B, nh, T, hs) -> (B, nh, T, hs)
        y = y.transpose(1, 2).contiguous().view(B, T, C) # re-assemble all head outputs side by side

        return y


class Block(nn.Module):
    """ an unassuming Transformer block """

    def __init__(self, n_embd, n_head, ):
        super().__init__()
        self.attn = SelfAttention(n_embd, n_head)

    def forward(self, x):

        x = self.attn(x)

        return x




class GPT(nn.Module):
    """  the full GPT language model, with a context size of block_size """

    def __init__(self, n_embd, n_head, n_layer, ):
        super().__init__()
        self.n_embd = n_embd

        # transformer
        self.blocks = nn.Sequential(*[Block(n_embd, n_head,)for layer in range(n_layer)])

        self._initialize_weights()

    def forward(self, image_tensor, thermal_tensor):
        """
        Args:
            image_tensor (tensor): B*4*1, C, H, W
            thermal_tensor (tensor): B*1, C, H, W
        """
        bz = thermal_tensor.shape[0]
        h, w = thermal_tensor.shape[2:4]
        # forward the image model for token embeddings
        image_tensor = image_tensor.view(bz, 1, -1, h, w)
        thermal_tensor = thermal_tensor.view(bz, 1, -1, h, w)
        # pad token embeddings along number of tokens dimension
        token_embeddings = torch.cat([image_tensor, thermal_tensor], dim=1).permute(0,1,3,4,2).contiguous()
        token_embeddings = token_embeddings.view(bz, -1, self.n_embd) # (B, an * T, C)
        x = token_embeddings
        x = self.blocks(x) # (B, an * T, C)
        tensor_out = x.view(bz, -1, h, w).contiguous()

        return tensor_out


    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight,)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)





class TranformerFusionNet(nn.Module):
    def __init__(self, ):
        super(TranformerFusionNet, self).__init__()

        self.transformer1 = GPT(n_embd=64,
                                n_head=4,
                                n_layer=1,
                                )
        self.outputConv = nn.Conv2d(2, 1, kernel_size=1)
        self._initialize_weights()

    def forward(self, density_1,density_2):
        mean_tensor=(density_1+density_2)/2
        out = self.transformer1(density_1, density_2)
        out = mean_tensor+self.outputConv(out)
        return out

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight,)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
```

取得了尚好的性能：12.720


顶多不超过5个echo就可以得到结果
加了TV——loss和GAN的方法之后性能一度在第一代就到了13以及在第二代到达15