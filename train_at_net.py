import os
import math
import time
import mmcv
import argparse
import torch.utils.data
from tqdm import tqdm
import torch.nn as nn
from datasets.crowdVideoDataset import CrowdVideoDataset,loader_wrapper
from torch.utils.data import DataLoader
from utils.utils import save_checkpoint,AverageMeter
from utils.logger import get_root_logger
from utils.seed import set_random_seed
from torch.autograd import Variable

from datasets.crowdDataset import CrowdDataset
from torch.utils.data import DataLoader
from models.at_net import at_net


parser = argparse.ArgumentParser(description='PyTorch CrowdCounting')
parser.add_argument('-train_path', default='/home/houyi/datasets/ShanghaiTech/part_B/train_data/ADJ3_ZOOM0.3_DR8_AUG18',)
parser.add_argument('-test_path', default='/home/houyi/datasets/ShanghaiTech/part_B/train_data/ADJ3_ZOOM0.3_DR8')
parser.add_argument('-lr',default=1e-5,type=float)
parser.add_argument('-scale_times',default=640.0,type=float)
# parser.add_argument('-resume_path',default='/home/houyi/Code/crowd_framework/work_dir/csrnet/20220219_142922.pth.tar',type=str)
parser.add_argument('-resume_path',default='',type=str)
parser.add_argument('-batch_size_train',default=50,type=int)
parser.add_argument('-batch_size_test',default=10,type=int)
parser.add_argument('-start_epoch',default=0,type=int)
parser.add_argument('-epochs',default=100,type=int)
parser.add_argument('-workers',default=10,type=int)
parser.add_argument('-task_id',default='at_net',type=str)

args = parser.parse_args()


def train(train_loader, model, criterion, optimizer, epoch):
    logger.info('Epoch: [{0}]\t'.format(epoch))
    losses = AverageMeter()
    model.train()
    for img, density_map, _ in tqdm(train_loader):

        img = Variable(img).cuda()
        density_map = Variable(density_map).cuda()*args.scale_times

        output1 = model(img)
        loss1 = criterion(output1, density_map)
        loss=loss1
        losses.update(loss.item(), img.size(0))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        logger.info('Loss_1 {losses_density.val:.8f} ({losses_density.avg:.8f})\t'.format(losses_density=losses))



def validate(val_loader, model):
    logger.info('begin test:')
    model.eval()
    mae1 = 0
    for i,(img, density, _) in enumerate(val_loader):
        with torch.no_grad():
            img = Variable(img).cuda()
            output1= model(img)
            output1=output1*(1.0/args.scale_times)
            density = Variable(density).cuda()
            diffs1=output1.data.sum(dim=[1,2,3])-density.data.sum(dim=[1,2,3])
            diffs1=diffs1.cpu().numpy()
            mae1 += sum(abs(diffs1))

    mae1 = mae1/(len(val_loader)*args.batch_size_test)
    logger.info(' * MAE_1 {mae1:.3f} '.format(mae1=mae1))
    return mae1



def adjust_learning_rate(optimizer, epoch):
    epochs_drop = 5
    args.lr = args.lr * math.pow(0.5, math.floor(epoch / epochs_drop))

    for param_group in optimizer.param_groups:
        param_group['lr'] = args.lr


if __name__ == '__main__':
    # 环境准备
    args.work_dir = os.path.join('./work_dirs',args.task_id)
    mmcv.mkdir_or_exist(os.path.abspath(args.work_dir))
    timestamp = time.strftime('%Y%m%d_%H%M%S', time.localtime())
    log_file = os.path.join(args.work_dir, f'{timestamp}.log')
    logger = get_root_logger(log_file=log_file)
    set_random_seed(1000)
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


    ##数据集准备
    train_dataset=CrowdDataset(args.train_path)
    val_dataset=CrowdDataset(args.test_path)
    train_loader = DataLoader(train_dataset, batch_size=args.batch_size_train, num_workers=args.workers, shuffle=True, drop_last=True)

    val_loader = DataLoader(val_dataset, batch_size=args.batch_size_test,num_workers=args.workers)

    model = at_net()
    model = model.cuda()
    criterion = nn.MSELoss().cuda()
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, weight_decay=1e-4)

    #主要训练流程准备
    best_prec1 = 1e6
    if args.resume_path:
        checkpoint = torch.load(args.resume_path)
        args.start_epoch = checkpoint['epoch']
        best_prec1 = checkpoint['best_prec1']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        logger.info("=> loaded checkpoint '{}' (epoch {})".format(args.resume_path, checkpoint['epoch']))

    for epoch in range(args.start_epoch, args.epochs):
        adjust_learning_rate(optimizer, epoch)

        train(train_loader, model, criterion, optimizer, epoch)#在这里训练一个批次
        prec1 = validate(val_loader, model)
        is_best = prec1 < best_prec1
        best_prec1 = min(prec1, best_prec1)
        logger.info(' * best MAE {mae:.3f} '.format(mae=best_prec1))
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'best_prec1': best_prec1,
            'optimizer' : optimizer.state_dict(),
        }, is_best=is_best,saving_dir=args.work_dir,saving_prefix=timestamp)


