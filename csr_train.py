import os
import math
import time
import mmcv
import argparse
import torch.utils.data
from tqdm import tqdm
from datasets.crowdVideoDataset import CrowdVideoDataset,loader_wrapper
from torch.utils.data import DataLoader
from utils.utils import save_checkpoint,AverageMeter
from models.frcc import FRCC,GeneratorLoss
from utils.logger import get_root_logger
from utils.seed import set_random_seed


parser = argparse.ArgumentParser(description='PyTorch CrowdCounting')
parser.add_argument('-train_path', default='/home/houyi/datasets/CrowdX_Video/train_data/ADJ3_ZOOM0.3_DR8_AUG18',)
parser.add_argument('-test_path', default='/home/houyi/datasets/CrowdX_Video/test_data/ADJ3_ZOOM0.3_DR8')
parser.add_argument('-lr',default=1e-4,type=float)
parser.add_argument('-scale_times',default=640.0,type=float)
# parser.add_argument('-resume_path',default='/home/houyi/Code/crowd_framework/work_dir/csrnet/20220219_142922.pth.tar',type=str)
parser.add_argument('-resume_path',default='',type=str)
parser.add_argument('-batch_size_train',default=5,type=int)
parser.add_argument('-batch_size_test',default=2,type=int)
parser.add_argument('-start_epoch',default=0,type=int)
parser.add_argument('-epochs',default=100,type=int)
parser.add_argument('-workers',default=10,type=int)
parser.add_argument('-task_id',default='csrnet',type=str)

args = parser.parse_args()


def train(train_loader, model, criterion, optimizer, epoch):
    logger.info('Epoch: [{0}]\t'.format(epoch))
    losses_density = AverageMeter()
    model.train()
    for img, density_map, _ in tqdm(train_loader):#[5，batch_size,channel,H,W]

        Loss_density = 0

        model.zero_grad()

        img = img.to(device)
        density_map = density_map.to(device)#*args.scale_times
        estDensityMap = model(img)
        loss_density= criterion(estDensityMap, density_map)
        Loss_density+=loss_density

        Loss_density /= len(imgs)
        Loss_density.backward()
        optimizer.step()
        losses_density.update(Loss_density.item())
        logger.info('Loss_1 {losses_density.val:.8f} ({losses_density.avg:.8f})\t'.format(losses_density=losses_density))

def validate(val_loader, model):
    logger.info('begin test:')
    model.eval()
    mae1 = 0
    with torch.no_grad():
        for imgs, density_maps, dot_maps in tqdm(val_loader):#[5，batch_size,channel,H,W]
            model.init_hidden(args.batch_size_test, img_height=768, img_width=1024)
            for img, density_map, _ in zip(imgs, density_maps, dot_maps):
                img = img.to(device)
                density_map = density_map.to(device)
                estDensityMap, _ = model(img)
                #estDensityMap=estDensityMap*(1.0/args.scale_times)
                diffs1=estDensityMap.data.sum(dim=[1,2,3])-density_map.data.sum(dim=[1,2,3])

                diffs1=diffs1.cpu().numpy()
                mae1 += sum(abs(diffs1))

    mae1 = mae1/(len(val_loader)*args.batch_size_test*5)
    logger.info(' * MAE_1 {mae1:.3f} '.format(mae1=mae1))
    return mae1


def adjust_learning_rate(optimizer, epoch):
    epochs_drop = 5
    args.lr = args.lr * math.pow(0.5, math.floor(epoch / epochs_drop))

    for param_group in optimizer.param_groups:
        param_group['lr'] = args.lr


if __name__ == '__main__':
    # 环境准备
    args.work_dir = os.path.join('./work_dirs',args.task_id)
    mmcv.mkdir_or_exist(os.path.abspath(args.work_dir))
    timestamp = time.strftime('%Y%m%d_%H%M%S', time.localtime())
    log_file = os.path.join(args.work_dir, f'{timestamp}.log')
    logger = get_root_logger(log_file=log_file)
    set_random_seed(1000)
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


    ##数据集准备
    train_dataset=CrowdVideoDataset(args.train_path,train_flag=True)
    val_dataset=CrowdVideoDataset(args.test_path,train_flag=False)
    train_loader = DataLoader(train_dataset, batch_size=args.batch_size_train, num_workers=args.workers, shuffle=True, drop_last=True)
    val_loader = DataLoader(val_dataset, batch_size=args.batch_size_test,num_workers=args.workers)    #[video_seq_size,batch_size,channel_num,W,H]

    train_loader = loader_wrapper(train_loader)
    val_loader = loader_wrapper(val_loader)

    #模型准备阶段
    model = FRCC(device=device).cuda()
    criterion = GeneratorLoss().cuda()
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, weight_decay=1e-4)

    best_prec1 = 1e6
    if args.resume_path:
        checkpoint = torch.load(args.resume_path)
        args.start_epoch = checkpoint['epoch']
        best_prec1 = checkpoint['best_prec1']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        logger.info("=> loaded checkpoint '{}' (epoch {})".format(args.resume_path, checkpoint['epoch']))

    for epoch in range(args.start_epoch, args.epochs):
        adjust_learning_rate(optimizer, epoch)

        train(train_loader, model, criterion, optimizer, epoch)#在这里训练一个批次
        prec1 = validate(val_loader, model)
        is_best = prec1 < best_prec1
        best_prec1 = min(prec1, best_prec1)
        print(' * best MAE {mae:.3f} '.format(mae=best_prec1))
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'best_prec1': best_prec1,
            'optimizer' : optimizer.state_dict(),
        }, is_best=is_best,saving_dir=args.work_dir,saving_prefix=timestamp)


